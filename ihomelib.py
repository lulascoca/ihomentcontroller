#!/usr/bin/python3

import pexpect

class Ihomeled():
  def __init__(self, device):
    self.child = pexpect.spawn("gatttool -I")
    self.device = device
    self.state = None

  def connect(self, maxTries):
    # Connect to the device.
    self.NOF_REMAINING_RETRY = maxTries
    while True:
      try:
        self.child.sendline("connect {0}".format(self.device))
        self.child.expect("Connection successful", timeout=5)
      except pexpect.TIMEOUT:
        NOF_REMAINING_RETRY = NOF_REMAINING_RETRY-1
        if (NOF_REMAINING_RETRY>0):
          print("timeout, retry...")
          continue
        else:
          print("timeout, giving up.")
          return 0
      else:
        self.state = self.keepAliveStatus()
        print("connected")
        return 1

  def keepAliveStatus(self):
    #returns 0 if the light is off and 1 if the lights are on
    try:
      self.child.sendline("char-write-cmd 0x0015 aa010000000000000000000000000000000000ab")
      self.child.expect("aa010000000000000000000000000000000000ab", timeout=1)
    except pexpect.TIMEOUT:
      return 1
    else:
      return 0

  def lightSet(self, value):
    #turns lights on or off depending on the boolean value of the state variable
    if value:
      try:
        self.child.sendline("char-write-cmd 0x0015 3301010000000000000000000000000000000033")
        self.child.expect("3301000000000000000000000000000000000032", timeout=1)
      except pexpect.TIMEOUT:
        return 0
      else:
        return 1
    else:
      try:
        self.child.sendline("char-write-cmd 0x0015 3301000000000000000000000000000000000032")
        self.child.expect("3301000000000000000000000000000000000032", timeout=1)
      except pexpect.TIMEOUT:
        return 0
      else:
        return 1

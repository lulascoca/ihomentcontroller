#!/usr/bin/python3

import pexpect
import time
import binascii
import sys
import ihomelib

device = "a4:c1:38:42:e2:7b"   # address of your device

if len(sys.argv) == 2:
  device = str(sys.argv[1])

Controller = ihomelib.Ihomeled(device)

if (not Controller.connect(3)):
  print("Error connecting to the device")
  exit(0)

if (Controller.state):
  Controller.lightSet(0)
  print("Turned lights off")
else:
  Controller.lightSet(1)
  print("Turned lights on")
